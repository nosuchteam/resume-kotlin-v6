package com.magnusbrorsson.android.resume.repository

import com.magnusbrorsson.android.resume.data.ContactDetails
import com.magnusbrorsson.android.resume.data.Introduction
import com.magnusbrorsson.android.resume.data.SectionItem
import com.magnusbrorsson.android.resume.data.Skill
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FakeRepository : Repository {

    override fun observeContactDetails(): Flow<State> = flow {
        emit(State.OnSuccess(contactDetails))
    }

    override fun observeIntroduction(): Flow<State> = flow {
        emit(State.OnSuccess(introduction))
    }

    override fun observeSkills(): Flow<State> = flow {
        emit(State.OnSuccess(skills))
    }

    override fun observeTimeline(): Flow<State> = flow {
        emit(State.OnSuccess(list))
    }

    override fun observeTimeline(type: String): Flow<State> = flow {
        emit(State.OnSuccess(list))
    }

    companion object {

        val contactDetails: ContactDetails = ContactDetails(
            "Firstname LastName",
            "123-456-789",
            "firstname.lastnam@home.com"
        )

        val introduction: Introduction = Introduction(
            "http://www.google.com",
            "Name",
            "Tagline",
            "Text"
        )

        val list: List<SectionItem> = arrayListOf(
            SectionItem(
                "description",
                "type",
                "http://www.google.com",
                "what",
                "who",
                "where",
                "start",
                "end",
                "lat",
                "lon"
            )
        )

        val skills: List<Skill> = arrayListOf(
            Skill(
                "experience",
                "level",
                "type",
                "what",
                "date"
            )
        )
    }
}
