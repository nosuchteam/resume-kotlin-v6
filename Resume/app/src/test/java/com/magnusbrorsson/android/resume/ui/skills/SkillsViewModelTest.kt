package com.magnusbrorsson.android.resume.ui.skills

import com.magnusbrorsson.android.resume.AppDispatchers
import com.magnusbrorsson.android.resume.data.Skill
import com.magnusbrorsson.android.resume.repository.FakeRepository
import com.magnusbrorsson.android.resume.repository.Repository
import com.magnusbrorsson.android.resume.repository.State
import com.magnusbrorsson.android.resume.util.MainCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import org.junit.*
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class SkillsViewModelTest {

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    private val repository = mock<Repository>()
    private val result: List<Skill> = FakeRepository.skills
    private val flow: MutableStateFlow<State> = MutableStateFlow(State.OnSuccess(result))

    private lateinit var viewModel: SkillsViewModel

    @Test
    fun `Loading state works`(): Unit = runBlocking {
        whenever(repository.observeSkills()).thenReturn(flow)
        viewModel = SkillsViewModel(
            repository,
            AppDispatchers(IO = StandardTestDispatcher())
        )
        Assert.assertEquals(
            State.Loading,
            viewModel.flow.value
        )
    }

    @Test
    fun `Success state works`() = runBlocking {
        whenever(repository.observeSkills()).thenReturn(flow)
        viewModel = SkillsViewModel(
            repository,
            AppDispatchers(IO = UnconfinedTestDispatcher())
        )
        Assert.assertEquals(
            State.OnSuccess(result),
            viewModel.flow.value
        )
    }
}
