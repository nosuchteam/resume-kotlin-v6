package com.magnusbrorsson.android.resume.ui.viewpager

import com.magnusbrorsson.android.resume.repository.FakeRepository
import org.junit.Assert
import org.junit.Test

class ViewPagerAdapterTest {

    @Test
    fun adapterSize() {
        val data =  FakeRepository.list
        val adapter = ViewPagerAdapter(data)
        Assert.assertEquals(
            "ItemAdapter is not the correct size",
            data.size,
            adapter.count
        )
    }
}
