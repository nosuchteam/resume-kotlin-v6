package com.magnusbrorsson.android.resume.ui.sections

import com.magnusbrorsson.android.resume.repository.FakeRepository
import org.junit.Assert
import org.junit.Test

class SectionAdapterTest {

    @Test
    fun adapterSize() {
        val data =  FakeRepository.list
        val adapter = SectionAdapter {}
        adapter.submitList(data)
        Assert.assertEquals(
            "ItemAdapter is not the correct size",
            data.size,
            adapter.itemCount
        )
    }
}
