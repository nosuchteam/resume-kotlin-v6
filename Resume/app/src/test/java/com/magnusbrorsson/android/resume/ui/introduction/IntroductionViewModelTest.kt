package com.magnusbrorsson.android.resume.ui.introduction

import com.magnusbrorsson.android.resume.AppDispatchers
import com.magnusbrorsson.android.resume.data.Introduction
import com.magnusbrorsson.android.resume.repository.*
import com.magnusbrorsson.android.resume.util.MainCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import org.junit.*
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class IntroductionViewModelTest {

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    private val repository = mock<Repository>()
    private val result: Introduction = FakeRepository.introduction
    private val flow: MutableStateFlow<State> = MutableStateFlow(State.OnSuccess(result))

    private lateinit var viewModel: IntroductionViewModel

    @Test
    fun `Loading state works`(): Unit = runBlocking {
        whenever(repository.observeIntroduction()).thenReturn(flow)
        viewModel = IntroductionViewModel(
            repository,
            AppDispatchers(IO = StandardTestDispatcher())
        )
        Assert.assertEquals(
            State.Loading,
            viewModel.flow.value
        )
    }

    @Test
    fun `Success state works`() = runBlocking {
        whenever(repository.observeIntroduction()).thenReturn(flow)
        viewModel = IntroductionViewModel(
            repository,
            AppDispatchers(IO = UnconfinedTestDispatcher())
        )
        Assert.assertEquals(
            State.OnSuccess(result),
            viewModel.flow.value
        )
    }
}
