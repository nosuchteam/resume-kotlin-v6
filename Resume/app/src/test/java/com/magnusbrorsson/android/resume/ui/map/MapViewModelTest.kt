package com.magnusbrorsson.android.resume.ui.map

import com.magnusbrorsson.android.resume.AppDispatchers
import com.magnusbrorsson.android.resume.data.SectionItem
import com.magnusbrorsson.android.resume.repository.FakeRepository
import com.magnusbrorsson.android.resume.repository.Repository
import com.magnusbrorsson.android.resume.repository.State
import com.magnusbrorsson.android.resume.util.MainCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class MapViewModelTest {

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    private val repository = mock<Repository>()
    private val result: List<SectionItem> = FakeRepository.list
    private val flow: MutableStateFlow<State> = MutableStateFlow(State.OnSuccess(result))

    private lateinit var viewModel: MapViewModel

    @Test
    fun `Loading state works`(): Unit = runBlocking {
        whenever(repository.observeTimeline()).thenReturn(flow)
        viewModel = MapViewModel(
            repository,
            AppDispatchers(IO = StandardTestDispatcher())
        )
        Assert.assertEquals(
            State.Loading,
            viewModel.flow.value
        )
    }

    @Test
    fun `Success state works`() = runBlocking {
        whenever(repository.observeTimeline()).thenReturn(flow)
        viewModel = MapViewModel(
            repository,
            AppDispatchers(IO = UnconfinedTestDispatcher())
        )
        Assert.assertEquals(
            State.OnSuccess(result),
            viewModel.flow.value)

    }
}
