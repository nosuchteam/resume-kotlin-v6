package com.magnusbrorsson.android.resume.repository

sealed class State {

    object Start : State()

    object Loading : State()


    data class OnSuccess(val value: Any) : State()


    data class OnError(val message: String) : State()
}
