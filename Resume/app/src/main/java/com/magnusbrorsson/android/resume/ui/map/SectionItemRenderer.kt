package com.magnusbrorsson.android.resume.ui.map

import android.content.Context
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.magnusbrorsson.android.resume.data.SectionItem

class SectionItemRenderer(
    context: Context?,
    map: GoogleMap?,
    clusterManager: ClusterManager<SectionItem>?
) : DefaultClusterRenderer<SectionItem>(
    context,
    map,
    clusterManager
) {

    override fun onBeforeClusterItemRendered(
        item: SectionItem,
        markerOptions: MarkerOptions
    ) {
        super.onBeforeClusterItemRendered(
            item,
            markerOptions
        )
        markerOptions.let {
            item.let { item ->
                it.position(
                    LatLng(
                        item.lat.toDouble(),
                        item.lon.toDouble()
                    )
                )
                it.title(item.what)
                it.snippet(
                    "${item.who}, ${item.where}"
                )
            }
        }
    }

    override fun onClusterItemRendered(
        clusterItem: SectionItem,
        marker: Marker
    ) {
        super.onClusterItemRendered(
            clusterItem,
            marker
        )
        marker.tag = clusterItem
    }
}
