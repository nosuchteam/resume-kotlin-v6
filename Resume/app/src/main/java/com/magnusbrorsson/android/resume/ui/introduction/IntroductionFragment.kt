package com.magnusbrorsson.android.resume.ui.introduction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.magnusbrorsson.android.resume.R
import com.magnusbrorsson.android.resume.data.Introduction
import com.magnusbrorsson.android.resume.databinding.FragmentIntroductionBinding
import com.magnusbrorsson.android.resume.repository.State
import com.magnusbrorsson.android.resume.showErrorSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class IntroductionFragment : Fragment() {

    private val viewModel by viewModels<IntroductionViewModel>()

    private lateinit var binding: FragmentIntroductionBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_introduction,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.flow.collect {
                when (it) {
                    is State.Loading -> {
                        binding.progressbar.visibility = View.VISIBLE
                    }
                    is State.OnSuccess -> {
                        binding.progressbar.visibility = View.GONE
                        binding.introduction = it.value as Introduction
                    }
                    is State.OnError -> {
                        binding.progressbar.visibility = View.GONE
                        binding.root.showErrorSnackbar(it.message)
                    }
                    else -> {}
                }
            }
        }
    }
}
