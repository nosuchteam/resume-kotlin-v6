package com.magnusbrorsson.android.resume.ui.viewpager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.magnusbrorsson.android.resume.databinding.FragmentViewpagerBinding

class ViewPagerFragment : Fragment() {

    private val args: ViewPagerFragmentArgs by navArgs()

    private lateinit var binding: FragmentViewpagerBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentViewpagerBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(
            view,
            savedInstanceState
        )
        binding.viewpager.adapter = ViewPagerAdapter(args.list.asList())
        binding.viewpager.currentItem = args.position
        binding.tabLayout.setupWithViewPager(
            binding.viewpager,
            true
        )
    }
}