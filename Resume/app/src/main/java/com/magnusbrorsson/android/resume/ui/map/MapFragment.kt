package com.magnusbrorsson.android.resume.ui.map

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.magnusbrorsson.android.resume.R
import com.magnusbrorsson.android.resume.data.SectionItem
import com.magnusbrorsson.android.resume.databinding.FragmentMapBinding
import com.magnusbrorsson.android.resume.repository.State
import com.magnusbrorsson.android.resume.showErrorSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MapFragment : Fragment(), OnMapReadyCallback {

    @Inject
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private val viewModel by viewModels<MapViewModel>()

    private lateinit var binding: FragmentMapBinding
    private lateinit var googleMap: GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMapBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(
        view: View, savedInstanceState: Bundle?
    ) {
        super.onViewCreated(
            view,
            savedInstanceState
        )
        binding.mapView.onCreate(savedInstanceState)
        binding.mapView.getMapAsync(this)
    }

    override fun onResume() {
        super.onResume()
        binding.mapView.onResume()
    }

    override fun onMapReady(map: GoogleMap) {
        this.googleMap = map
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.flow.collect {
                when (it) {
                    is State.Loading -> {
                        binding.progressbar.visibility = View.VISIBLE
                    }
                    is State.OnSuccess -> {
                        binding.progressbar.visibility = View.GONE
                        addClusteredMarkers(it.value as List<SectionItem>)
                    }
                    is State.OnError -> {
                        binding.progressbar.visibility = View.GONE
                        binding.root.showErrorSnackbar(it.message)
                    }
                    else -> {}
                }
            }
        }
        getDeviceLocation()
    }

    private fun addClusteredMarkers(items: List<SectionItem>?) {
        val clusterManager = ClusterManager<SectionItem>(
            requireContext(),
            googleMap
        )
        clusterManager.renderer = SectionItemRenderer(
            requireContext(),
            googleMap,
            clusterManager
        )
        clusterManager.addItems(items)
        clusterManager.cluster()
        googleMap.setOnCameraIdleListener {
            clusterManager.onCameraIdle()
        }
        googleMap.setOnCameraMoveStartedListener {
            clusterManager.markerCollection.markers.forEach {
                it.alpha = 0.3f
            }
            clusterManager.clusterMarkerCollection.markers.forEach {
                it.alpha = 0.3f
            }
        }
        googleMap.setOnCameraIdleListener {
            clusterManager.markerCollection.markers.forEach {
                it.alpha = 1.0f
            }
            clusterManager.clusterMarkerCollection.markers.forEach {
                it.alpha = 1.0f
            }
            clusterManager.onCameraIdle()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getDeviceLocation() {
        fusedLocationProviderClient.lastLocation.addOnCompleteListener {
            if (it.isSuccessful) {
                val lastKnownLocation = it.result
                lastKnownLocation?.let {
                    googleMap.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(
                                lastKnownLocation.latitude,
                                lastKnownLocation.longitude
                            ),
                            DEFAULT_ZOOM
                        )
                    )
                    createHomeMarkerFor(lastKnownLocation)
                }
            } else {
                binding.root.showErrorSnackbar("Error message to be decided")
            }
        }
    }

    private fun createHomeMarkerFor(location: Location) {
        val latLng = LatLng(
            location.latitude,
            location.longitude
        )
        AppCompatResources.getDrawable(
            requireContext(),
            R.drawable.ic_home_black_24px
        )?.let {
            val markerOptions: MarkerOptions = MarkerOptions().apply {
                position(latLng)
                title(getString(R.string.text_you_are_here))
                icon(getMarkerIconFrom(it))
            }
            googleMap.addMarker(markerOptions)?.showInfoWindow()
            googleMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    latLng,
                    1f
                )
            )
        }
    }

    private fun getMarkerIconFrom(drawable: Drawable): BitmapDescriptor {
        val bitmap = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        drawable.setBounds(
            0,
            0,
            drawable.intrinsicWidth,
            drawable.intrinsicHeight)
        drawable.draw(Canvas().apply {
            setBitmap(bitmap)
        })
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    companion object {

        private const val DEFAULT_ZOOM: Float = 15.0F
    }
}