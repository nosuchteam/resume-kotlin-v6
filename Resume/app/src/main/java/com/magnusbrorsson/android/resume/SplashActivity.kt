package com.magnusbrorsson.android.resume

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import com.magnusbrorsson.android.resume.databinding.ActivitySplashBinding
import com.magnusbrorsson.android.resume.ui.MainActivity

class SplashActivity : AppCompatActivity(R.layout.activity_splash) {

    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        onRequestPermissions()
    }

    private fun onRequestPermissions() {
        if (hasPermissions()) {
            startMainActivity()
        } else {
            ActivityCompat.requestPermissions(
                this,
                PERMISSIONS, REQUEST_PERMISSIONS
            )
        }
    }

    private fun hasPermissions(): Boolean {
        for (item in PERMISSIONS) {
            if (ActivityCompat.checkSelfPermission(
                    applicationContext,
                    item
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_PERMISSIONS -> processGrantResults(grantResults)
            else -> super.onRequestPermissionsResult(
                requestCode,
                permissions,
                grantResults
            )
        }
    }

    private fun processGrantResults(grantResults: IntArray) {
        grantResults.forEach {
            if (grantResults.isNotEmpty() && it != PackageManager.PERMISSION_GRANTED) {
                binding.root.showErrorSnackbar(getString(R.string.location_permission_denied))
                return
            }
        }
        startMainActivity()
    }

    private fun startMainActivity() {
        finish()
        startActivity(Intent(this, MainActivity::class.java))
    }

    companion object {

        private const val REQUEST_PERMISSIONS: Int = 1
        private val PERMISSIONS = arrayOf(ACCESS_COARSE_LOCATION)
    }
}
