package com.magnusbrorsson.android.resume.repository

import com.google.firebase.database.*
import com.google.firebase.database.ktx.getValue
import com.magnusbrorsson.android.resume.data.ContactDetails
import com.magnusbrorsson.android.resume.data.Introduction
import com.magnusbrorsson.android.resume.data.SectionItem
import com.magnusbrorsson.android.resume.data.Skill
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

@ExperimentalCoroutinesApi
class RepositoryImpl(private val database: FirebaseDatabase) : Repository {

    private val reference = database.reference

    override fun observeContactDetails(): Flow<State> = callbackFlow {
        val listener = object : ValueEventListener {

            override fun onDataChange(snapshot: DataSnapshot) {
                trySend(
                    element = State.OnSuccess(
                        value = snapshot.child(CONTACT_DETAILS_REFERENCE)
                            .getValue<ContactDetails>() as ContactDetails
                    )
                ).isSuccess
            }

            override fun onCancelled(error: DatabaseError) {
                trySend(element = State.OnError(message = error.message)).isSuccess
            }
        }
        reference.addValueEventListener(listener)
        awaitClose {
            reference.removeEventListener(listener)
        }
    }

    override fun observeIntroduction(): Flow<State> = callbackFlow {
        val listener = object : ValueEventListener {

            override fun onDataChange(snapshot: DataSnapshot) {
                trySend(
                    element = State.OnSuccess(
                        value = snapshot.child(INTRODUCTION_REFERENCE)
                            .getValue<Introduction>() as Introduction
                    )
                ).isSuccess
            }

            override fun onCancelled(error: DatabaseError) {
                trySend(element = State.OnError(message = error.message)).isSuccess
            }
        }
        reference.addValueEventListener(listener)
        awaitClose {
            reference.removeEventListener(listener)
        }
    }

    override fun observeSkills(): Flow<State> = callbackFlow {
        val listener = object : ValueEventListener {

            override fun onDataChange(snapshot: DataSnapshot) {
                trySend(
                    element = State.OnSuccess(
                        value = getSkills(snapshot = snapshot.child(SKILLS_REFERENCE))
                    )
                ).isSuccess
            }

            override fun onCancelled(error: DatabaseError) {
                trySend(element = State.OnError(message = error.message)).isSuccess
            }
        }
        reference.addValueEventListener(listener)
        awaitClose {
            reference.removeEventListener(listener)
        }
    }

    internal fun getSkills(snapshot: DataSnapshot?): List<Skill> {
        return ArrayList<Skill>(0).apply {
            snapshot?.let {
                addAll(snapshot.children.map {
                    it.child("skill").getValue(Skill::class.java) ?: Skill()
                })
                sortedWith(compareBy({ it.level }, { it.date }))
            }
        }
    }

    override fun observeTimeline(): Flow<State> = callbackFlow {
        val listener = object : ValueEventListener {

            override fun onDataChange(snapshot: DataSnapshot) {
                trySend(
                    element = State.OnSuccess(
                        value = getSection(snapshot = snapshot.child(TIMELINE_REFERENCE))
                    )
                ).isSuccess
            }

            override fun onCancelled(error: DatabaseError) {
                trySend(element = State.OnError(message = error.message)).isSuccess
            }
        }
        reference.addValueEventListener(listener)
        awaitClose {
            reference.removeEventListener(listener)
        }
    }

    internal fun getSection(snapshot: DataSnapshot?) = ArrayList<SectionItem>(0).apply {
        snapshot?.let {
            addAll(snapshot.children.map {
                it.child("item").getValue(SectionItem::class.java) ?: SectionItem()
            })
        }
    }

    override fun observeTimeline(type: String): Flow<State> = callbackFlow {
        val listener = object : ValueEventListener {

            override fun onDataChange(snapshot: DataSnapshot) {
                trySend(
                    element = State.OnSuccess(
                        value = getSection(
                            snapshot = snapshot.child(TIMELINE_REFERENCE),
                            type = type
                        )
                    )
                ).isSuccess
            }

            override fun onCancelled(error: DatabaseError) {
                trySend(element = State.OnError(message = error.message)).isSuccess
            }
        }
        reference.addValueEventListener(listener)
        awaitClose {
            reference.removeEventListener(listener)
        }
    }

    internal fun getSection(
        snapshot: DataSnapshot?,
        type: String
    ): List<SectionItem> {
        val list: ArrayList<SectionItem> = ArrayList(0)
        snapshot?.let {
            list.addAll(snapshot.children.filter {
                val sectionItem: SectionItem = it.child("item")
                    .getValue(SectionItem::class.java) ?: SectionItem()
                sectionItem.type == type
            }.map {
                it.child("item").getValue(SectionItem::class.java) ?: SectionItem()
            })
            list.sortedWith(compareBy { it.start })
        }
        return list
    }

    companion object {

        const val CONTACT_DETAILS_REFERENCE: String = "contact_details"
        const val INTRODUCTION_REFERENCE: String = "introduction"
        const val SKILLS_REFERENCE: String = "skills"
        const val TIMELINE_REFERENCE: String = "timeline"
    }
}
