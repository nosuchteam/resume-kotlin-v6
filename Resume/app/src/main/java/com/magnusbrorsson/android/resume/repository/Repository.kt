package com.magnusbrorsson.android.resume.repository

import kotlinx.coroutines.flow.Flow

interface Repository {

    fun observeContactDetails(): Flow<State>

    fun observeIntroduction(): Flow<State>

    fun observeSkills(): Flow<State>

    fun observeTimeline(): Flow<State>

    fun observeTimeline(type: String): Flow<State>
}
