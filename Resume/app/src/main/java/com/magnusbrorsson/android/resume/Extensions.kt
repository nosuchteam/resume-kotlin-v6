package com.magnusbrorsson.android.resume

import android.text.TextUtils
import android.view.View
import android.webkit.WebView
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.storage.FirebaseStorage

fun View.showErrorSnackbar(message: String) {
    val snackbar: Snackbar = Snackbar.make(
        this,
        message,
        Snackbar.LENGTH_INDEFINITE
    )
    snackbar.setAction(android.R.string.ok) { snackbar.dismiss() }
    snackbar.show()
}

@BindingAdapter("url")
fun ImageView.loadImageInto(url: String?) {
    when {
        TextUtils.isEmpty(url) -> loadNoImage()
        else -> loadURLImageInto(url ?: "")
    }
}

private fun ImageView.loadNoImage() {
    Glide.with(context)
        .load(R.drawable.ic_broken_image)
        .into(this)
}

private fun ImageView.loadURLImageInto(url: String) {
    FirebaseStorage.getInstance().reference.child(url).downloadUrl
        .addOnSuccessListener {
            Glide.with(context)
                .load(it).apply(
                    RequestOptions()
                        .placeholder(R.drawable.loading_animation)
                        .error(R.drawable.ic_broken_image)
                )
                .into(this)
        }
        .addOnFailureListener { loadNoImage() }
}

@BindingAdapter("data")
fun WebView.loadDescriptionInto(data: String?) {
    loadDataWithBaseURL(
        "",
        data ?: "",
        "text/html",
        "utf-8",
        ""
    )
}
