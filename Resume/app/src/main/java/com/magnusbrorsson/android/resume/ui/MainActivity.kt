package com.magnusbrorsson.android.resume.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.navigation.findNavController
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ShareCompat
import androidx.navigation.NavController
import androidx.navigation.ui.*
import com.magnusbrorsson.android.resume.R
import com.magnusbrorsson.android.resume.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }
    private val navController: NavController by lazy {
        findNavController(R.id.nav_host_fragment_content_main)
    }

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setSupportActionBar(binding.appBarMain.toolbar)
        // Passing each menu ID as a set of Ids because each menu should be considered as top level
        // destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_introduction,
                R.id.nav_employment,
                R.id.nav_projects,
                R.id.nav_schools,
                R.id.nav_courses,
                R.id.nav_certifications,
                R.id.nav_skills,
                R.id.nav_volunteering,
                R.id.nav_map
            ), binding.drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        binding.navView.setupWithNavController(navController)
        binding.appBarMain.fab.setOnClickListener {
            navController.navigate(R.id.contact_details_fragment)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.action_share -> {
            ShareCompat.IntentBuilder(this).setType("text/plain").startChooser()
            true
        }
        else -> NavigationUI.onNavDestinationSelected(
            item,
            navController
        ) || super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean =
        navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
}
