package com.magnusbrorsson.android.resume.ui.skills

import android.os.Bundle
import android.view.*
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.magnusbrorsson.android.resume.R
import com.magnusbrorsson.android.resume.data.Skill
import com.magnusbrorsson.android.resume.databinding.FragmentSkillsBinding
import com.magnusbrorsson.android.resume.repository.State
import com.magnusbrorsson.android.resume.showErrorSnackbar
import dagger.hilt.android.AndroidEntryPoint
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SkillsFragment : Fragment(), MenuProvider {

    private val viewModel by viewModels<SkillsViewModel>()

    private lateinit var binding: FragmentSkillsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSkillsBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(
            view,
            savedInstanceState
        )
        requireActivity().addMenuProvider(this, viewLifecycleOwner)
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.flow.collect {
                when (it) {
                    is State.Loading -> {
                        binding.progressbar.visibility = View.VISIBLE
                    }
                    is State.OnSuccess -> {
                        binding.progressbar.visibility = View.GONE
                        binding.recyclerView.adapter = getSectionedAdapter(it.value as List<Skill>)
                    }
                    is State.OnError -> {
                        binding.progressbar.visibility = View.GONE
                        binding.root.showErrorSnackbar(it.message)
                    }
                    else -> {}
                }
            }
        }
    }

    private fun getSectionedAdapter(skills: List<Skill>) = SectionedRecyclerViewAdapter().apply {
        addSection(
            SkillSection(
                getString(R.string.title_languages),
                skills.filter {
                    it.type == "language"
                })
        )
        addSection(
            SkillSection(
                getString(R.string.title_frameworks),
                skills.filter {
                    it.type == "framework"
                })
        )
        addSection(
            SkillSection(
                getString(R.string.title_tools),
                skills.filter {
                    it.type == "tool"
                })
        )
        addSection(
            SkillSection(
                getString(R.string.title_os),
                skills.filter {
                    it.type == "os"
                })
        )
        addSection(
            SkillSection(
                getString(R.string.title_technologies),
                skills.filter {
                    it.type == "technology"
                })
        )
        addSection(
            SkillSection(
                getString(R.string.title_processes),
                skills.filter {
                    it.type == "process"
                })
        )
    }

    override fun onCreateMenu(
        menu: Menu,
        menuInflater: MenuInflater
    ) {
        menuInflater.inflate(
            R.menu.menu_skills,
            menu
        )
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean = when (menuItem.itemId) {
        R.id.action_legend -> {
            showAlertDialog()
            true
        }
        else -> super.onContextItemSelected(menuItem)
    }

    private fun showAlertDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.action_legend)
            .setView(R.layout.view_skills_legend)
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
            .show()
    }
}
