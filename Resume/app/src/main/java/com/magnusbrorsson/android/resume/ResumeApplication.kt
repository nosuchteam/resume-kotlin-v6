package com.magnusbrorsson.android.resume

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ResumeApplication : Application()