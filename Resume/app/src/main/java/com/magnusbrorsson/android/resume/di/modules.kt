package com.magnusbrorsson.android.resume.di

import android.content.Context
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.database.FirebaseDatabase
import com.magnusbrorsson.android.resume.AppDispatchers
import com.magnusbrorsson.android.resume.repository.Repository
import com.magnusbrorsson.android.resume.repository.RepositoryImpl
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi

@dagger.Module
@InstallIn(SingletonComponent::class)
@OptIn(ExperimentalCoroutinesApi::class)
object Module {

    @Provides
    fun provideFirebase(): FirebaseDatabase = FirebaseDatabase.getInstance()

    @Provides
    fun provideRepository(database: FirebaseDatabase): Repository = RepositoryImpl(database)

    @Provides
    fun provideAppDispatcher(): AppDispatchers = AppDispatchers()

    @Provides
    fun provideLocationProviderClient(@ApplicationContext appContext: Context): FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(appContext)
}
