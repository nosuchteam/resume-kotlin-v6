package com.magnusbrorsson.android.resume.data

data class Introduction(
    val url: String = "",
    val name: String = "",
    val tagline: String = "",
    val text: String = ""
)
