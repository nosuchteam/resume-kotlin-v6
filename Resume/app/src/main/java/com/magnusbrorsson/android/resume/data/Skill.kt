package com.magnusbrorsson.android.resume.data

data class Skill(
    val experience: String = "",
    val level: String = "",
    val type: String = "",
    val what: String = "",
    val date: String = ""
)
