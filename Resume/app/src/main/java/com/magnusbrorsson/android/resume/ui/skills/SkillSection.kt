package com.magnusbrorsson.android.resume.ui.skills

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.magnusbrorsson.android.resume.R
import com.magnusbrorsson.android.resume.data.Skill
import io.github.luizgrp.sectionedrecyclerviewadapter.Section
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters

class SkillSection(
    private val title: String,
    private val skills: List<Skill>
) : Section(
    SectionParameters.builder()
        .headerResourceId(R.layout.view_skill_header)
        .itemResourceId(R.layout.view_skills_item)
        .build()
) {

    override fun getContentItemsTotal(): Int = skills.size

    override fun getHeaderViewHolder(view: View): RecyclerView.ViewHolder =
        SkillsHeaderViewHolder(view)

    override fun getItemViewHolder(view: View): RecyclerView.ViewHolder = SkillsItemViewHolder(view)

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder?) {
        super.onBindHeaderViewHolder(holder)
        (holder as SkillsHeaderViewHolder).bind(title)
    }

    override fun onBindItemViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        super.onBindHeaderViewHolder(holder)
        (holder as SkillsItemViewHolder).bind(skills[position])
    }


    class SkillsHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val titleTextView: TextView =
            itemView.findViewById<View>(R.id.text_title) as TextView

        fun bind(title: String) {
            titleTextView.text = title
        }
    }


    class SkillsItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val whatTextView: TextView = itemView.findViewById<View>(R.id.text_what) as TextView
        private val experienceTextView: TextView =
            itemView.findViewById<View>(R.id.text_experience) as TextView
        private val levelTextView: TextView =
            itemView.findViewById<View>(R.id.text_level) as TextView
        private val whenTextView: TextView = itemView.findViewById<View>(R.id.text_when) as TextView

        fun bind(skill: Skill) {
            whatTextView.text = skill.what
            experienceTextView.text = skill.experience
            levelTextView.text = skill.level
            whenTextView.text = skill.date
        }
    }
}
