package com.magnusbrorsson.android.resume.ui.sections

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.magnusbrorsson.android.resume.data.SectionItem
import com.magnusbrorsson.android.resume.databinding.FragmentSectionBinding
import com.magnusbrorsson.android.resume.repository.State
import com.magnusbrorsson.android.resume.showErrorSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch

@AndroidEntryPoint
abstract class SectionFragment(private val type: String) : Fragment() {

    private val viewModel by viewModels<SectionViewModel>()

    private lateinit var list: List<SectionItem>
    private lateinit var binding: FragmentSectionBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSectionBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = SectionAdapter {
            findNavController().navigate(
                SectionFragmentDirections.actionSectionFragmentToViewpagerFragment(
                    list.toTypedArray(),
                    it
                )
            )
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.flow.collect {
                when (it) {
                    is State.Loading -> {
                        binding.progressbar.visibility = View.VISIBLE
                    }
                    is State.OnSuccess -> {
                        binding.progressbar.visibility = View.GONE
                        binding.recyclerView.adapter = adapter
                        list = it.value as List<SectionItem>
                        adapter.submitList(list)
                    }
                    is State.OnError -> {
                        binding.progressbar.visibility = View.GONE
                        binding.root.showErrorSnackbar(it.message)
                    }
                    else -> {}
                }
            }
        }
        viewModel.onLoadData(type)
    }
}

@ExperimentalCoroutinesApi
class EmploymentFragment : SectionFragment("employment")

@ExperimentalCoroutinesApi
class ProjectsFragment : SectionFragment("project")

@ExperimentalCoroutinesApi
class SchoolsFragment : SectionFragment("school")

@ExperimentalCoroutinesApi
class CoursesFragment : SectionFragment("course")

@ExperimentalCoroutinesApi
class CertificatesFragment : SectionFragment("certificate")

@ExperimentalCoroutinesApi
class VolunteerWorkFragment : SectionFragment("volunteer_work")
