package com.magnusbrorsson.android.resume.ui.skills

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.magnusbrorsson.android.resume.AppDispatchers
import com.magnusbrorsson.android.resume.repository.Repository
import com.magnusbrorsson.android.resume.repository.State
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SkillsViewModel @Inject constructor(
    private val repository: Repository,
    private val appDispatchers: AppDispatchers
) : ViewModel() {

    val flow: MutableStateFlow<State?> = MutableStateFlow(State.Start)

    init {
        viewModelScope.launch {
            flow.value = State.Loading
            withContext(context = appDispatchers.IO) {
                repository.observeSkills().collect {
                    flow.value = it
                }
            }
        }
    }
}
