package com.magnusbrorsson.android.resume.data

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem
import kotlinx.parcelize.Parcelize

@Parcelize
data class SectionItem(
    val description: String = "",
    val type: String = "",
    val url: String = "",
    val what: String = "",
    val who: String = "",
    val where: String = "",
    val start: String = "",
    val end: String = "",
    val lat: String = "",
    val lon: String = ""
) : Parcelable, ClusterItem {

    override fun getPosition(): LatLng = LatLng(
        lat.toDouble(),
        lon.toDouble()
    )

    override fun getTitle(): String = "$type $what"

    override fun getSnippet(): String = "$start $end"
}
