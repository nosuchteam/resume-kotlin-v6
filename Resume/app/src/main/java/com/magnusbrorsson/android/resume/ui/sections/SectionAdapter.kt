package com.magnusbrorsson.android.resume.ui.sections

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.magnusbrorsson.android.resume.data.SectionItem
import com.magnusbrorsson.android.resume.databinding.ViewSectionItemBinding

class SectionAdapter(private val onItemClick: ((Int) -> Unit)) :
    ListAdapter<SectionItem, SectionAdapter.SectionItemViewHolder>(SectionDiffCallback()) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SectionItemViewHolder = SectionItemViewHolder.from(parent)

    override fun onBindViewHolder(
        holder: SectionItemViewHolder,
        position: Int
    ) {
        holder.bind(
            getItem(position),
            position,
            onItemClick
        )
    }

    class SectionDiffCallback : DiffUtil.ItemCallback<SectionItem>() {

        override fun areItemsTheSame(
            oldItem: SectionItem,
            newItem: SectionItem
        ): Boolean = oldItem.who == newItem.who

        override fun areContentsTheSame(
            oldItem: SectionItem,
            newItem: SectionItem
        ): Boolean = oldItem.who == newItem.who
    }


    class SectionItemViewHolder private constructor(private val binding: ViewSectionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: SectionItem,
            position: Int,
            onItemClick: ((Int) -> Unit)
        ) {
            binding.item = item
            binding.root.setOnClickListener {
                onItemClick(position)
            }
            binding.executePendingBindings()
        }

        companion object {

            fun from(parent: ViewGroup): SectionItemViewHolder = SectionItemViewHolder(
                ViewSectionItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }
}
