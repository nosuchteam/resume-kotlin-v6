package com.magnusbrorsson.android.resume.ui.viewpager

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.magnusbrorsson.android.resume.data.SectionItem
import com.magnusbrorsson.android.resume.databinding.ViewPagerItemBinding

class ViewPagerAdapter(private val list: List<SectionItem>) : PagerAdapter() {

    override fun getCount(): Int = list.size

    override fun isViewFromObject(
        view: View,
        `object`: Any
    ): Boolean = view === `object`

    override fun instantiateItem(
        container: ViewGroup,
        position: Int
    ): View {
        val binding = ViewPagerItemBinding.inflate(LayoutInflater.from(container.context))
        binding.item = list[position]
        container.addView(binding.root)
        return binding.root
    }

    override fun destroyItem(
        container: ViewGroup,
        position: Int,
        `object`: Any
    ) {
        container.removeView(`object` as View)
    }
}
