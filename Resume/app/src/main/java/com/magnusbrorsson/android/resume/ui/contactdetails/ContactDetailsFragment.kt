package com.magnusbrorsson.android.resume.ui.contactdetails

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.magnusbrorsson.android.resume.R
import com.magnusbrorsson.android.resume.data.ContactDetails
import com.magnusbrorsson.android.resume.databinding.FragmentContactDetailsBinding
import com.magnusbrorsson.android.resume.repository.State
import com.magnusbrorsson.android.resume.showErrorSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ContactDetailsFragment : DialogFragment() {

    private val viewModel by viewModels<ContactDetailsViewModel>()

    private lateinit var binding: FragmentContactDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentContactDetailsBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setTitle(R.string.contact_details)
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.flow.collect {
                when (it) {
                    is State.OnSuccess -> {
                        val contactDetails: ContactDetails = it.value as ContactDetails
                        binding.contactDetailsName.text = contactDetails.name
                        binding.buttonPhone.setOnClickListener {
                            startActivityWith(contactDetails.getPhoneIntent())
                        }
                        binding.buttonEmail.setOnClickListener {
                            startActivityWith(contactDetails.getEmailIntent())
                        }
                    }
                    is State.OnError -> {
                        binding.root.showErrorSnackbar(it.message)
                    }
                    else -> {}
                }
            }
        }
    }

    private fun startActivityWith(intent: Intent) {
        try {
            dismiss()
            startActivity(intent)
        } catch (e: Exception) {
            binding.root.showErrorSnackbar(e.message ?: getString(R.string.error_general))
        }
    }
}
