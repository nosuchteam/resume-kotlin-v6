package com.magnusbrorsson.android.resume.data

import android.content.Intent
import android.net.Uri
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ContactDetails(
    var name: String = "",
    private var phone: String = "",
    private var email: String = ""
) : Parcelable {

    fun getPhoneIntent(): Intent = createIntent(
        action = Intent.ACTION_DIAL,
        uriString = "tel:${phone}"
    )

    fun getEmailIntent(): Intent = createIntent(
        action = Intent.ACTION_SENDTO,
        uriString = "mailto:${email}"
    )

    private fun createIntent(
        action: String,
        uriString: String
    ): Intent = Intent(action).apply {
        data = Uri.parse(uriString)
    }
}
